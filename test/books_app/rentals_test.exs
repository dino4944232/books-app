defmodule BooksApp.RentalsTest do
  use BooksApp.DataCase

  alias BooksApp.Rentals
  alias BooksApp.Users.User
  alias BooksApp.Books.Book
  alias BooksApp.Vendors.Vendor
  alias BooksApp.Repo

  defp get_book() do
    case Repo.get(Book, 1) do
      nil ->
        vendor = Repo.insert!(%Vendor{name: "Vendor"})
        Repo.insert!(%Book{title: "Title", vendor_id: vendor.id})
      book -> book
    end
  end


  defp get_user() do
    case Repo.get(Vendor, 1) do
      nil -> Repo.insert!(%User{name: "User Name"})
      user -> user
    end
  end

  describe "rentals" do
    alias BooksApp.Rentals.Rental

    import BooksApp.RentalsFixtures

    @invalid_attrs %{rental_day: nil}

    test "list_rentals/0 returns all rentals" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      assert Rentals.list_rentals() == [rental |> Repo.preload([:user, :book])]
    end

    test "get_rental!/1 returns the rental with given id" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      assert Rentals.get_rental!(rental.id) == rental |> Repo.preload([:user, :book])
    end

    test "create_rental/1 with valid data creates a rental" do
      valid_attrs = %{rental_day: ~D[2023-08-09], price: 10}

      assert {:ok, %Rental{} = rental} = Rentals.create_rental(valid_attrs)
      assert rental.rental_day == ~D[2023-08-09]
    end

    test "create_rental/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rentals.create_rental(@invalid_attrs)
    end

    test "update_rental/2 with valid data updates the rental" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      update_attrs = %{rental_day: ~D[2023-08-10]}

      assert {:ok, %Rental{} = rental} = Rentals.update_rental(rental, update_attrs)
      assert rental.rental_day == ~D[2023-08-10]
    end

    test "update_rental/2 with invalid data returns error changeset" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      assert {:error, %Ecto.Changeset{}} = Rentals.update_rental(rental, @invalid_attrs)
      assert rental |> Repo.preload([:user, :book]) == Rentals.get_rental!(rental.id)
    end

    test "delete_rental/1 deletes the rental" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      assert {:ok, %Rental{}} = Rentals.delete_rental(rental)
      assert_raise Ecto.NoResultsError, fn -> Rentals.get_rental!(rental.id) end
    end

    test "change_rental/1 returns a rental changeset" do
      rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
      assert %Ecto.Changeset{} = Rentals.change_rental(rental)
    end
  end
end
