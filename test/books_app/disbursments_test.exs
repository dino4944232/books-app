defmodule BooksApp.DisbursmentsTest do
  use BooksApp.DataCase

  alias BooksApp.Disbursments
  alias BooksApp.Repo
  alias BooksApp.Vendors.Vendor

  defp get_vendor() do
    case Repo.get(Vendor, 1) do
      nil -> Repo.insert!(%BooksApp.Vendors.Vendor{name: "Vendor Name"})
      vendor -> vendor
    end
  end


  describe "disbursments" do
    alias BooksApp.Disbursments.Disbursment

    import BooksApp.DisbursmentsFixtures

    @invalid_attrs %{amount: nil, month: nil, year: nil}

    test "list_disbursments/0 returns all disbursments" do
      disbursment = disbursment_fixture(%{vendor_id: get_vendor().id})
      assert Disbursments.list_disbursments() == [disbursment |> Repo.preload(:vendor)]
    end


    test "create_disbursment/1 with valid data creates a disbursment" do
      valid_attrs = %{amount: 42, month: 42, year: 42}

      assert {:ok, %Disbursment{} = disbursment} = Disbursments.create_disbursment(valid_attrs)
      assert disbursment.amount == Decimal.new(42)
      assert disbursment.month == 42
      assert disbursment.year == 42
    end

    test "create_disbursment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Disbursments.create_disbursment(@invalid_attrs)
    end


  end
end
