defmodule BooksApp.DisbursmentsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BooksApp.Disbursments` context.
  """

  @doc """
  Generate a disbursment.
  """
  def disbursment_fixture(attrs \\ %{}) do
    {:ok, disbursment} =
      attrs
      |> Enum.into(%{
        amount: Decimal.new(42),
        month: 7,
        year: 2023
      })
      |> BooksApp.Disbursments.create_disbursment()

    disbursment
  end
end
