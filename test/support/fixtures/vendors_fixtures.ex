defmodule BooksApp.VendorsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BooksApp.Vendors` context.
  """

  @doc """
  Generate a vendor.
  """
  def vendor_fixture(attrs \\ %{}) do
    {:ok, vendor} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> BooksApp.Vendors.create_vendor()

    vendor
  end
end
