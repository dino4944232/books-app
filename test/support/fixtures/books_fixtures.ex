defmodule BooksApp.BooksFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BooksApp.Books` context.
  """

  @doc """
  Generate a book.
  """
  def book_fixture(attrs \\ %{}) do
    {:ok, book} =
      attrs
      |> Enum.into(%{
        title: "some title"
      })
      |> BooksApp.Books.create_book()

    book |> BooksApp.Repo.preload(:vendor)
  end
end
