defmodule BooksApp.RentalsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BooksApp.Rentals` context.
  """

  @doc """
  Generate a rental.
  """
  def rental_fixture(attrs \\ %{}) do
    {:ok, rental} =
      attrs
      |> Enum.into(%{
        rental_day: ~D[2023-08-10],
        price: 10
      })
      |> BooksApp.Rentals.create_rental()

    rental
  end
end
