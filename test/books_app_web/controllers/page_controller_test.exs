defmodule BooksAppWeb.PageControllerTest do
  use BooksAppWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, ~p"/")
    assert conn == conn
  end
end
