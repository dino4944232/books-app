defmodule BooksAppWeb.DisbursmentLiveTest do
  use BooksAppWeb.ConnCase

  import Phoenix.LiveViewTest
  import BooksApp.DisbursmentsFixtures

  alias BooksApp.Repo
  alias BooksApp.Disbursments.{Calculate, Disbursment}
  alias BooksApp.Vendors.Vendor
  alias BooksApp.Books.Book
  alias BooksApp.Users.User
  alias BooksApp.Rentals.Rental

  defp get_vendor() do
    case Repo.get(Vendor, 1) do
      nil -> Repo.insert!(%BooksApp.Vendors.Vendor{name: "Vendor Name"})
      vendor -> vendor
    end
  end

  defp create_disbursment(_) do
    disbursment = disbursment_fixture(%{vendor: get_vendor().id})
    %{disbursment: disbursment}
  end

  defp now() do
    DateTime.utc_now() |> DateTime.truncate(:second) |>  DateTime.to_naive
  end

  describe "Index" do
    setup [:create_disbursment]

    test "lists all disbursments", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/disbursments")

      assert html =~ "Listing Disbursments"
    end


  end

  describe "Show" do
    setup [:create_disbursment]

    test "displays disbursment", %{conn: conn, disbursment: disbursment} do
      {:ok, _show_live, html} = live(conn, ~p"/disbursments/#{disbursment}")

      assert html =~ "Show Disbursment"
    end

  end

  describe "Calculate" do
    test "calculate disbursment", %{conn: conn} do

      user = Repo.insert!(%User{name: "User name"})
      vendor = Repo.insert!(%Vendor{name: "Vendor name"})
      book = Repo.insert!(%Book{title: "Book title", vendor_id: vendor.id})

      rentals = [
        %{user_id: user.id, book_id: book.id, price: 10, rental_day: ~D[2023-07-12], inserted_at: now(), updated_at: now()  },
        %{user_id: user.id, book_id: book.id, price: 15, rental_day: ~D[2023-07-15], inserted_at: now(), updated_at: now() },
        %{user_id: user.id, book_id: book.id, price: 1, rental_day: ~D[2023-07-09], inserted_at: now(), updated_at: now() },
        %{user_id: user.id, book_id: book.id, price: 100, rental_day: ~D[2023-07-08], inserted_at: now(), updated_at: now() },
        %{user_id: user.id, book_id: book.id, price: 35, rental_day: ~D[2023-07-07], inserted_at: now(), updated_at: now() }
      ]

     Repo.insert_all(Rental, rentals)

    list_all = Repo.all(Rental) |> Repo.preload(book: :vendor)

    Calculate.do_calculations(list_all, ~D[2023-07-10])

     result =
      Repo.all(Disbursment) |> List.first()

      assert result.amount == Decimal.new("2.43")
    end
  end

end
