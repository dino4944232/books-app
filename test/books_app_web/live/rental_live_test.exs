defmodule BooksAppWeb.RentalLiveTest do
  use BooksAppWeb.ConnCase

  import Phoenix.LiveViewTest
  import BooksApp.RentalsFixtures
  alias BooksApp.Users.User
  alias BooksApp.Books.Book
  alias BooksApp.Vendors.Vendor
  alias BooksApp.Repo

  @moduletag timeout: :infinity

  @create_attrs %{rental_day: "2023-08-10", price: 10}
  @update_attrs %{rental_day: "2023-08-11",  price: 15}
  @invalid_attrs %{rental_day: nil,  price: nil}


  defp get_book() do
    case Repo.get(Book, 1) do
      nil ->
        vendor = Repo.insert!(%Vendor{name: "Vendor"})
        Repo.insert!(%Book{title: "Title", vendor_id: vendor.id})
      book -> book
    end
  end


  defp get_user() do
    case Repo.get(Vendor, 1) do
      nil -> Repo.insert!(%User{name: "User Name"})
      user -> user
    end
  end


  defp create_rental(_) do
    rental = rental_fixture(%{user_id: get_user().id, book_id: get_book().id})
    %{rental: rental}
  end

  describe "Index" do
    setup [:create_rental]

    test "lists all rentals", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/rentals")

      assert html =~ "Listing Rentals"
    end

    test "saves new rental", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/rentals")

      assert index_live |> element("a", "New Rental") |> render_click() =~
               "New Rental"

      assert_patch(index_live, ~p"/rentals/new")

      assert index_live
             |> form("#rental-form", rental: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#rental-form", rental: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/rentals")

      html = render(index_live)
      assert html =~ "Rental created successfully"
    end

    test "updates rental in listing", %{conn: conn, rental: rental} do
      {:ok, index_live, _html} = live(conn, ~p"/rentals")

      assert index_live |> element("#rentals-#{rental.id} a", "Edit") |> render_click() =~
               "Edit Rental"

      assert_patch(index_live, ~p"/rentals/#{rental}/edit")

      assert index_live
             |> form("#rental-form", rental: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#rental-form", rental: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/rentals")

      html = render(index_live)
      assert html =~ "Rental updated successfully"
    end

    test "deletes rental in listing", %{conn: conn, rental: rental} do
      {:ok, index_live, _html} = live(conn, ~p"/rentals")

      assert index_live |> element("#rentals-#{rental.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#rentals-#{rental.id}")
    end
  end

  describe "Show" do
    setup [:create_rental]

    test "displays rental", %{conn: conn, rental: rental} do
      {:ok, _show_live, html} = live(conn, ~p"/rentals/#{rental}")

      assert html =~ "Show Rental"
    end

    test "updates rental within modal", %{conn: conn, rental: rental} do
      {:ok, show_live, _html} = live(conn, ~p"/rentals/#{rental}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Rental"

      assert_patch(show_live, ~p"/rentals/#{rental}/show/edit")

      assert show_live
             |> form("#rental-form", rental: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#rental-form", rental: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/rentals/#{rental}")

      html = render(show_live)
      assert html =~ "Rental updated successfully"
    end
  end
end
