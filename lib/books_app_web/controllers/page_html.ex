defmodule BooksAppWeb.PageHTML do
  use BooksAppWeb, :html

  embed_templates "page_html/*"
end
