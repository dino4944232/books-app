defmodule BooksAppWeb.DisbursmentLive.FormComponent do
  use BooksAppWeb, :live_component

  alias BooksApp.Disbursments

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage disbursment records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="disbursment-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:amount]} type="number" label="Amount" />
        <.input field={@form[:month]} type="number" label="Month" />
        <.input field={@form[:year]} type="number" label="Year" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Disbursment</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{disbursment: disbursment} = assigns, socket) do
    changeset = Disbursments.change_disbursment(disbursment)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"disbursment" => disbursment_params}, socket) do
    changeset =
      socket.assigns.disbursment
      |> Disbursments.change_disbursment(disbursment_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"disbursment" => disbursment_params}, socket) do
    save_disbursment(socket, socket.assigns.action, disbursment_params)
  end

  defp save_disbursment(socket, :edit, disbursment_params) do
    case Disbursments.update_disbursment(socket.assigns.disbursment, disbursment_params) do
      {:ok, disbursment} ->
        notify_parent({:saved, disbursment})

        {:noreply,
         socket
         |> put_flash(:info, "Disbursment updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_disbursment(socket, :new, disbursment_params) do
    case Disbursments.create_disbursment(disbursment_params) do
      {:ok, disbursment} ->
        notify_parent({:saved, disbursment})

        {:noreply,
         socket
         |> put_flash(:info, "Disbursment created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
