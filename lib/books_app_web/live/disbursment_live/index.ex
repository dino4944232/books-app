defmodule BooksAppWeb.DisbursmentLive.Index do
  use BooksAppWeb, :live_view

  alias BooksApp.Disbursments
  alias BooksApp.Disbursments.Disbursment
  alias BooksApp.Vendors
  require Logger


  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Disbursment")
    |> assign(:disbursment, Disbursments.get_disbursment!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Disbursment")
    |> assign(:disbursment, %Disbursment{})
  end

  defp apply_action(socket, :index, _params) do
    vendors = Vendors.list_vendors() |> Enum.map(&{&1.name, &1.id})

    changeset =
      Disbursments.change_disbursment(%Disbursment{})

    form = to_form(changeset)

    months = 1..12

    socket
    |> assign(:page_title, "Listing Disbursments")
    |> assign(:disbursment, nil)
    |> assign(:vendors, vendors)
    |> assign(:changeset, changeset)
    |> assign(:form, form)
    |> assign(:months, months)
    |> assign(:disbursments, Disbursments.filtered_result(%{}))
  end

  def handle_event("validate", %{"disbursment" => disbursment_params}, socket) do
    disbursments = Disbursments.filtered_result(disbursment_params)

    {:noreply, assign(socket, :disbursments, disbursments)}
  end


  @impl true
  def handle_info({BooksAppWeb.DisbursmentLive.FormComponent, {:saved, disbursment}}, socket) do
    {:noreply, stream_insert(socket, :disbursments, disbursment)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    disbursment = Disbursments.get_disbursment!(id)
    {:ok, _} = Disbursments.delete_disbursment(disbursment)

    {:noreply, stream_delete(socket, :disbursments, disbursment)}
  end
end
