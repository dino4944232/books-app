defmodule BooksAppWeb.DisbursmentLive.Show do
  use BooksAppWeb, :live_view

  alias BooksApp.Disbursments

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:disbursment, Disbursments.get_disbursment!(id))}
  end

  defp page_title(:show), do: "Show Disbursment"
  defp page_title(:edit), do: "Edit Disbursment"
end
