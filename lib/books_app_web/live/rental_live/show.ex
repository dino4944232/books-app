defmodule BooksAppWeb.RentalLive.Show do
  use BooksAppWeb, :live_view

  alias BooksApp.Rentals

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:rental, Rentals.get_rental!(id))}
  end

  defp page_title(:show), do: "Show Rental"
  defp page_title(:edit), do: "Edit Rental"
end
