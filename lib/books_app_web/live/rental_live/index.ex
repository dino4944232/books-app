defmodule BooksAppWeb.RentalLive.Index do
  use BooksAppWeb, :live_view

  alias BooksApp.Rentals
  alias BooksApp.Rentals.Rental

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :rentals, Rentals.list_rentals())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Rental")
    |> assign(:rental, Rentals.get_rental!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Rental")
    |> assign(:rental, %Rental{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Rentals")
    |> assign(:rental, nil)
  end

  @impl true
  def handle_info({BooksAppWeb.RentalLive.FormComponent, {:saved, rental}}, socket) do
    {:noreply, stream_insert(socket, :rentals, rental)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    rental = Rentals.get_rental!(id)
    {:ok, _} = Rentals.delete_rental(rental)

    {:noreply, stream_delete(socket, :rentals, rental)}
  end
end
