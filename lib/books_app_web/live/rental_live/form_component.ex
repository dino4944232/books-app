defmodule BooksAppWeb.RentalLive.FormComponent do
  use BooksAppWeb, :live_component

  alias BooksApp.Rentals
  alias BooksApp.Users
  alias BooksApp.Books

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage rental records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="rental-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:rental_day]} type="date" label="Rental day" />
        <.input field={@form[:price]} type="number" min="1" label="Price" />
        <.input field={@form[:user_id]} type="select" options={@users}  label="User" />
        <.input field={@form[:book_id]} type="select" options={@books}  label="Book" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Rental</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{rental: rental} = assigns, socket) do
    changeset = Rentals.change_rental(rental)
    users = Users.list_users() |> Enum.map(&{&1.name, &1.id})
    books = Books.list_books() |> Enum.map(&{&1.title, &1.id})

    {:ok,
     socket
     |> assign(assigns)
     |> assign([users: users, books: books])
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"rental" => rental_params}, socket) do
    changeset =
      socket.assigns.rental
      |> Rentals.change_rental(rental_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"rental" => rental_params}, socket) do
    save_rental(socket, socket.assigns.action, rental_params)
  end

  defp save_rental(socket, :edit, rental_params) do
    case Rentals.update_rental(socket.assigns.rental, rental_params) do
      {:ok, rental} ->
        rental = rental |> BooksApp.Repo.preload([:user, :book])
        notify_parent({:saved, rental})

        {:noreply,
         socket
         |> put_flash(:info, "Rental updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_rental(socket, :new, rental_params) do
    case Rentals.create_rental(rental_params) do
      {:ok, rental} ->
        rental = rental |> BooksApp.Repo.preload([:user, :book])
        notify_parent({:saved, rental})

        {:noreply,
         socket
         |> put_flash(:info, "Rental created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
