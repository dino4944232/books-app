defmodule BooksAppWeb.Layouts do
  use BooksAppWeb, :html

  embed_templates "layouts/*"
end
