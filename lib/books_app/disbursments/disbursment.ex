defmodule BooksApp.Disbursments.Disbursment do
  use Ecto.Schema
  import Ecto.Changeset
  alias BooksApp.Vendors.Vendor
  schema "disbursments" do
    field :amount, :decimal
    field :month, :integer
    field :year, :integer

    belongs_to :vendor, Vendor

    timestamps()
  end

  @doc false
  def changeset(disbursment, attrs) do
    disbursment
    |> cast(attrs, [:amount, :month, :year, :vendor_id])
    |> validate_required([:amount, :month, :year])
    |> unique_constraint(:unique_month_year, name: :month_year_index)
  end
end
