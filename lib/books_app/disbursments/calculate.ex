defmodule BooksApp.Disbursments.Calculate do

  alias BooksApp.Rentals.Rental
  alias BooksApp.Repo
  alias BooksApp.Disbursments
  import Ecto.Query
  require Logger
  def calc() do
    today = Date.utc_today()

    last_month = previous_month(today)


    first_day = Date.beginning_of_month(last_month)
    last_day = Date.end_of_month(last_month)

    list_all_rentals =
      Rental
        |> where([r], fragment("?::date", r.rental_day) >= ^first_day and  fragment("?::date", r.rental_day) <= ^last_day )
        |> Repo.all()
        |> Repo.preload(book: :vendor)

    do_calculations(list_all_rentals, last_month)
  end

  def do_calculations(list_all_rentals, last_month) do
    list =
      for r <- list_all_rentals do
        %{
          fee: calc_amount(r.price),
          book_id: r.book_id,
          vendor_id: r.book.vendor.id,
          price: r.price,
          rental_day: r.rental_day
        }
      end

    group_by_vendor =
      Enum.group_by(list, fn x -> x.vendor_id end)


    grouped_result =
      Enum.map(group_by_vendor, fn {x, m} ->

        fees_for_vendor =
          Enum.map(m, fn x -> x.fee end)

        sum =
          Enum.reduce(fees_for_vendor, fn x, acc -> Decimal.add(x, acc) end)

        %{
          vendor_id: x,
          amount: sum
        }

      end)

    for g <- grouped_result do
      Disbursments.create_disbursment(%{amount: g.amount, vendor_id: g.vendor_id, year: last_month.year, month: last_month.month})
    end
  end


  defp calc_amount(price) do
    price_in_dec = Decimal.new(price)

    first_cond =
      Decimal.new(Decimal.from_float(2/100)) # 2%

    second_cond =
      Decimal.new(Decimal.from_float(1.5/100)) #1.5%

    third_cond =
      Decimal.new(Decimal.from_float(1/100)) #1%


    cond do
      Decimal.compare(price_in_dec, 10) == :lt ->
        Decimal.mult(price_in_dec, first_cond) |> Decimal.round(2)

      Decimal.compare(price_in_dec, 100) == :gt ->
        Decimal.mult(price_in_dec, third_cond) |> Decimal.round(2)

      Decimal.compare(price_in_dec, 10) in [:eq, :gt] and Decimal.compare(price_in_dec, 100) in [:eq, :lt] ->
        Decimal.mult(price_in_dec, second_cond) |> Decimal.round(2)

    end
  end


  defp previous_month(%Date{day: day} = date) do
    days = max(day, (Date.add(date, -day)).day)
    Date.add(date, -days)
  end

end
