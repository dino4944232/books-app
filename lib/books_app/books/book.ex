defmodule BooksApp.Books.Book do
  use Ecto.Schema
  import Ecto.Changeset
  alias BooksApp.Vendors.Vendor

  schema "books" do
    field :title, :string


    belongs_to :vendor , Vendor

    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:title, :vendor_id])
    |> validate_required([:title, :vendor_id])
  end
end
