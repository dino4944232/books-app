defmodule BooksApp.Disbursments do
  @moduledoc """
  The Disbursments context.
  """

  import Ecto.Query, warn: false
  alias BooksApp.Repo

  alias BooksApp.Disbursments.Disbursment


  @spec filtered_result(nil | maybe_improper_list | map) :: any
  def filtered_result(params) do
    vendor_id = params["vendor_id"]
    month = params["month"]

    Disbursment
    |> filter_vendor(vendor_id)
    |> filter_month(month)
    |> Repo.all()
    |> Repo.preload(:vendor)
  end

  defp filter_vendor(query, vendor_id) when vendor_id in ["", nil], do: query
  defp filter_vendor(query, vendor_id) do
    query
    |> where([d], d.vendor_id == ^vendor_id)
  end

  defp filter_month(query, month) when month in ["", nil], do: query
  defp filter_month(query, month) do
    query
    |> where([d], d.month == ^month)
  end


  @doc """
  Returns the list of disbursments.

  ## Examples

      iex> list_disbursments()
      [%Disbursment{}, ...]

  """
  def list_disbursments do
    Repo.all(Disbursment) |> Repo.preload(:vendor)
  end

  @doc """
  Gets a single disbursment.

  Raises `Ecto.NoResultsError` if the Disbursment does not exist.

  ## Examples

      iex> get_disbursment!(123)
      %Disbursment{}

      iex> get_disbursment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_disbursment!(id), do: Repo.get!(Disbursment, id) |> Repo.preload(:vendor)

  @doc """
  Creates a disbursment.

  ## Examples

      iex> create_disbursment(%{field: value})
      {:ok, %Disbursment{}}

      iex> create_disbursment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_disbursment(attrs \\ %{}) do
    %Disbursment{}
    |> Disbursment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a disbursment.

  ## Examples

      iex> update_disbursment(disbursment, %{field: new_value})
      {:ok, %Disbursment{}}

      iex> update_disbursment(disbursment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_disbursment(%Disbursment{} = disbursment, attrs) do
    disbursment
    |> Disbursment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a disbursment.

  ## Examples

      iex> delete_disbursment(disbursment)
      {:ok, %Disbursment{}}

      iex> delete_disbursment(disbursment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_disbursment(%Disbursment{} = disbursment) do
    Repo.delete(disbursment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking disbursment changes.

  ## Examples

      iex> change_disbursment(disbursment)
      %Ecto.Changeset{data: %Disbursment{}}

  """
  def change_disbursment(%Disbursment{} = disbursment, attrs \\ %{}) do
    Disbursment.changeset(disbursment, attrs)
  end
end
