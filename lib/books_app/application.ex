defmodule BooksApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      BooksAppWeb.Telemetry,
      # Start the Ecto repository
      BooksApp.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: BooksApp.PubSub},
      # Start Finch
      {Finch, name: BooksApp.Finch},
      # Start the Endpoint (http/https)
      BooksAppWeb.Endpoint
      # Start a worker by calling: BooksApp.Worker.start_link(arg)
      # {BooksApp.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BooksApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    BooksAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
