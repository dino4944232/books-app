defmodule BooksApp.Rentals.Rental do
  use Ecto.Schema
  import Ecto.Changeset

  alias BooksApp.Books.Book
  alias BooksApp.Users.User

  schema "rentals" do
    field :rental_day, :date
    field :price, :integer

    belongs_to :book, Book
    belongs_to :user, User
    timestamps()
  end

  @doc false
  def changeset(rental, attrs) do
    rental
    |> cast(attrs, [:rental_day, :price, :book_id, :user_id])
    |> validate_required([:rental_day, :price])
  end
end
