defmodule Mix.Tasks.Calculate do
  require Logger
  use Mix.Task

  def run(_) do
    Mix.Task.run("app.start")

    BooksApp.Disbursments.Calculate.calc()

  end
end
