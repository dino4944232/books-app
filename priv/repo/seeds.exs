# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BooksApp.Repo.insert!(%BooksApp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

defmodule SeedHelpers do
  def get_random_book() do
    Enum.random(1..150)
  end

  def get_random_vendor() do
    Enum.random(1..50)
  end

  def get_random_user() do
    Enum.random(1..100)
  end

  def get_random_price() do
    Enum.random(1..263)
  end

  def get_random_date() do
    range = Date.range(~D[2023-06-24], ~D[2023-08-10])
    Enum.random(range)
  end
end


alias BooksApp.Repo
alias BooksApp.Books.Book
alias BooksApp.Rentals.Rental
alias BooksApp.Users.User
alias BooksApp.Vendors.Vendor
alias SeedHelpers

for x <- 1..50 do
  BooksApp.Repo.insert!(%Vendor{name: "Vendor_#{x}"})
end

for x <- 1..100 do
  BooksApp.Repo.insert!(%User{name: "User_#{x}"})
end

for x <- 1..150 do
  BooksApp.Repo.insert!(%Book{title: "Book_#{x}", vendor_id: SeedHelpers.get_random_vendor()})
end

for x <- 1..500 do
  BooksApp.Repo.insert!(
    %Rental{
      user_id: SeedHelpers.get_random_user(),
      book_id: SeedHelpers.get_random_book(),
      price: SeedHelpers.get_random_price(),
      rental_day: SeedHelpers.get_random_date()
    }
  )
end
