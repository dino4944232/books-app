defmodule BooksApp.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:books) do
      add :title, :string
      add :vendor_id, references(:vendors, on_delete: :nothing)

      timestamps()
    end

    create index(:books, [:vendor_id])
  end
end
