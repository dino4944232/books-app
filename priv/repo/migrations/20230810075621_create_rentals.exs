defmodule BooksApp.Repo.Migrations.CreateRentals do
  use Ecto.Migration

  def change do
    create table(:rentals) do
      add :rental_day, :date
      add :book_id, references(:books, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)
      add :price, :integer
      timestamps()
    end

    create index(:rentals, [:book_id])
    create index(:rentals, [:user_id])
  end
end
