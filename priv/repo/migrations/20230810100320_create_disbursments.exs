defmodule BooksApp.Repo.Migrations.CreateDisbursments do
  use Ecto.Migration

  def change do
    create table(:disbursments) do
      add :amount, :decimal
      add :month, :integer
      add :year, :integer
      add :vendor_id, references(:vendors, on_delete: :nothing)

      timestamps()
    end

    create index(:disbursments, [:vendor_id])

    create unique_index(:disbursments, [:month, :year, :vendor_id], name: :month_year_index)
  end
end
